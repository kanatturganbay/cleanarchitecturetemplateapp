package thousand.group.data.storage.constants

object StorageConstants {
    const val PREF_NO_VAL = "PREF_NO_VAL"
    const val PREF_ACCESS_TOKEN = "PREF_ACCESS_TOKEN"
    const val PREF_SAVE_USER = "PREF_SAVE_USER"
    const val PREF_LANG_DEFAULT_VAL = "ru"
    const val PREF_LANG_CODE = "PREF_LANG_CODE"
    const val PREF_PERM_AUDIO = "PREF_PERM_AUDIO"
    const val PREF_PERM_PUSH = "PREF_PERM_PUSH"
    const val PREF_SAVED_USER_MODEL = "PREF_SAVED_USER_MODEL"
    const val PREF_FIRST_LAUNCHED = "PREF_FIRST_LAUNCHED"
    const val PREF_PUSH_RECEIVED = "PREF_PUSH_RECEIVED"
}