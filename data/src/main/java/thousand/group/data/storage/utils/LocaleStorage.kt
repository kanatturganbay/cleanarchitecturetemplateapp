package thousand.group.data.storage.utils

interface LocaleStorage {

    fun setAccessToken(accessToken: String)

    fun getAccessToken(): String

    fun saveUser(saveUser: Boolean)

    fun isUserSaved(): Boolean

    fun setLanguage(lang: String)

    fun getLanguage(): String

    fun setSound(sound: Int)

    fun getSound(): Int

    fun setPush(push: Int)

    fun getPush(): Int

//    fun saveUserModel(user: User)
//
//
//    fun deleteUserModel()
//
//    fun getUserModel(): User? {
//        val modelJson = Prefs.getString(PREF_SAVED_USER_MODEL, null)
//
//        modelJson?.apply {
//            return GsonHelper.getObjectFromString(this, User::class.java)
//        }
//
//        return null
//    }

    fun setFirstTimeLaunched(firstTimeLaunched: Boolean)

    fun isFirstTimeLaunched(): Boolean

    fun setPushReceived(received: Boolean)

    fun isPushReceived(): Boolean
}