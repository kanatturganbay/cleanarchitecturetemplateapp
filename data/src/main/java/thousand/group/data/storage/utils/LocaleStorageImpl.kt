package thousand.group.data.storage.utils

import android.content.ContextWrapper
import com.pixplicity.easyprefs.library.Prefs
import thousand.group.data.storage.constants.StorageConstants.PREF_ACCESS_TOKEN
import thousand.group.data.storage.constants.StorageConstants.PREF_FIRST_LAUNCHED
import thousand.group.data.storage.constants.StorageConstants.PREF_LANG_CODE
import thousand.group.data.storage.constants.StorageConstants.PREF_LANG_DEFAULT_VAL
import thousand.group.data.storage.constants.StorageConstants.PREF_NO_VAL
import thousand.group.data.storage.constants.StorageConstants.PREF_PERM_AUDIO
import thousand.group.data.storage.constants.StorageConstants.PREF_PERM_PUSH
import thousand.group.data.storage.constants.StorageConstants.PREF_PUSH_RECEIVED
import thousand.group.data.storage.constants.StorageConstants.PREF_SAVE_USER

class LocaleStorageImpl : LocaleStorage {

    override fun setAccessToken(accessToken: String) =
        Prefs.putString(PREF_ACCESS_TOKEN, accessToken)

    override fun getAccessToken(): String = Prefs.getString(PREF_ACCESS_TOKEN, PREF_NO_VAL)

    override fun saveUser(saveUser: Boolean) = Prefs.putBoolean(PREF_SAVE_USER, saveUser)

    override fun isUserSaved(): Boolean = Prefs.getBoolean(PREF_SAVE_USER, false)

    override fun setLanguage(lang: String) = Prefs.putString(PREF_LANG_CODE, lang)

    override fun getLanguage(): String = Prefs.getString(PREF_LANG_CODE, PREF_LANG_DEFAULT_VAL)

    override fun setSound(sound: Int) = Prefs.putInt(PREF_PERM_AUDIO, sound)

    override fun getSound(): Int = Prefs.getInt(PREF_PERM_AUDIO, 1)

    override fun setPush(push: Int) = Prefs.putInt(PREF_PERM_PUSH, push)

    override fun getPush(): Int = Prefs.getInt(PREF_PERM_PUSH, 1)

//    fun saveUserModel(user: User) =
//        Prefs.putString(PREF_SAVED_USER_MODEL, GsonHelper.getJsonFromObject(user))
//
//    fun deleteUserModel() = Prefs.remove(PREF_SAVED_USER_MODEL)
//
//    fun getUserModel(): User? {
//        val modelJson = Prefs.getString(PREF_SAVED_USER_MODEL, null)
//
//        modelJson?.apply {
//            return GsonHelper.getObjectFromString(this, User::class.java)
//        }
//
//        return null
//    }

    override fun setFirstTimeLaunched(firstTimeLaunched: Boolean) =
        Prefs.putBoolean(PREF_FIRST_LAUNCHED, firstTimeLaunched)

    override fun isFirstTimeLaunched(): Boolean = Prefs.getBoolean(PREF_FIRST_LAUNCHED, true)

    override fun setPushReceived(received: Boolean) = Prefs.putBoolean(PREF_PUSH_RECEIVED, received)

    override fun isPushReceived(): Boolean = Prefs.getBoolean(PREF_PUSH_RECEIVED, false)

}