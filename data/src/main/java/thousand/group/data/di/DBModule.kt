package thousand.group.data.di

import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import thousand.group.data.db.utils.DataBaseUtil

val dbModule = module {
    single { DataBaseUtil.create(androidContext()) }
}