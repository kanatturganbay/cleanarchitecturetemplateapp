package thousand.group.data.di

import org.koin.dsl.module
import thousand.group.data.repositories.main.MainRepository
import thousand.group.data.repositories.main.MainRepositoryImpl
import thousand.group.data.repositories.registration.RegistrationRepository
import thousand.group.data.repositories.registration.RegistrationRepositoryImpl
import thousand.group.data.repositories.user_info.UserInfoRepository
import thousand.group.data.repositories.user_info.UserInfoRepositoryImpl

val repModule = module {
    single<RegistrationRepository> { RegistrationRepositoryImpl(get(), get(), get()) }
    single<UserInfoRepository> { UserInfoRepositoryImpl(get(), get(), get()) }
    single<MainRepository> { MainRepositoryImpl(get(), get(), get()) }
}