package thousand.group.data.di

import android.content.ContextWrapper
import com.pixplicity.easyprefs.library.Prefs
import org.koin.android.ext.koin.androidContext
import org.koin.dsl.module
import thousand.group.data.storage.utils.LocaleStorage
import thousand.group.data.storage.utils.LocaleStorageImpl

val storageModule = module {

    single<LocaleStorage> { LocaleStorageImpl() }
}