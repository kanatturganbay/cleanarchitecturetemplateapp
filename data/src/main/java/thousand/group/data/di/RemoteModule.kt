package thousand.group.data.di

import org.koin.dsl.module
import thousand.group.data.remote.utils.RemoteBuilder.createOkHttpClient
import thousand.group.data.remote.utils.RemoteBuilder.createWebService
import thousand.group.data.remote.constants.RemoteBuilderConstants.SERVER_URL
import thousand.group.data.remote.utils.RemoteService

val remoteModule = module {
    single { createOkHttpClient() }
    single { createWebService<RemoteService>(get(), SERVER_URL) }
}