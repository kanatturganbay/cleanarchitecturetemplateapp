package thousand.group.data.db.utils

import android.content.Context
import androidx.room.Database
import androidx.room.Room
import androidx.room.RoomDatabase
import thousand.group.data.db.constants.DbConstants
import thousand.group.data.entities.db.UserInfoEntity

@Database(
    entities = [
        UserInfoEntity::class
    ],
    version = 1,
    exportSchema = false
)
abstract class DataBaseUtil : RoomDatabase() {
    abstract fun getDataDao(): DataDao

    companion object {

        private const val TAG = "DataBaseUtil"

        fun create(context: Context): DataBaseUtil =
            Room.databaseBuilder(
                context,
                DataBaseUtil::class.java,
                DbConstants.DB_NAME
            )
                .build()
    }
}