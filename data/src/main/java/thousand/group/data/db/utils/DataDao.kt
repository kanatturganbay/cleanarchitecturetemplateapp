package thousand.group.data.db.utils

import androidx.room.Dao
import androidx.room.Insert
import androidx.room.OnConflictStrategy
import androidx.room.Query
import thousand.group.data.entities.db.UserInfoEntity

@Dao
interface DataDao {

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    suspend fun addUserInfo(userInfoModel: UserInfoEntity)

    @Query("select * from user_info where id=:id")
    suspend fun getUserInfo(id: Long): MutableList<UserInfoEntity>

}