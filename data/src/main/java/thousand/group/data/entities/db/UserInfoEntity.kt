package thousand.group.data.entities.db

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.Index
import androidx.room.PrimaryKey

@Entity(
    tableName = "user_info",
    indices = [Index(
        value = ["local_id"],
        unique = true
    )]
)
data class UserInfoEntity(

    @PrimaryKey(autoGenerate = true)
    @ColumnInfo(name = "local_id")
    var local_id: Long = 0,

    @ColumnInfo(name = "id")
    var id: Long = 0,

    @ColumnInfo(name = "username")
    var username: String? = null,

    @ColumnInfo(name = "firstName")
    var firstName: String? = null,

    @ColumnInfo(name = "lastName")
    var lastName: String? = null,

    @ColumnInfo(name = "email")
    var email: String? = null,

    @ColumnInfo(name = "password")
    var password: String? = null,

    @ColumnInfo(name = "phone")
    var phone: String? = null,

    @ColumnInfo(name = "userStatus")
    var userStatus: Int? = 0
)