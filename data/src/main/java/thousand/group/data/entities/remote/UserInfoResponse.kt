package thousand.group.data.entities.remote

data class UserInfoResponse(
    var id: Long,
    var username: String,
    var firstName: String,
    var lastName: String,
    var email: String,
    var password: String,
    var phone: String,
    var userStatus: Int
)