package thousand.group.data.repositories.registration

import com.google.gson.JsonObject
import thousand.group.data.db.utils.DataBaseUtil
import thousand.group.data.remote.utils.RemoteService
import thousand.group.data.storage.utils.LocaleStorage

class RegistrationRepositoryImpl(
    private val remoteService: RemoteService,
    private val dbUtil: DataBaseUtil,
    private val storage: LocaleStorage
) : RegistrationRepository {

    override suspend fun register(userData: JsonObject): JsonObject =
        remoteService.register(userData)
}