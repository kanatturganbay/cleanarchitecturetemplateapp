package thousand.group.data.repositories.registration

import com.google.gson.JsonObject

interface RegistrationRepository {
    suspend fun register(userData: JsonObject): JsonObject
}