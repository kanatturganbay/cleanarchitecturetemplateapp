package thousand.group.data.repositories.main

import thousand.group.data.db.utils.DataBaseUtil
import thousand.group.data.remote.utils.RemoteService
import thousand.group.data.storage.utils.LocaleStorage

class MainRepositoryImpl(
    private val remoteService: RemoteService,
    private val dbUtil: DataBaseUtil,
    private val storage: LocaleStorage
) : MainRepository {

    override fun setLanguage(lang: String) = storage.setLanguage(lang)

    override fun getLanguage(): String = storage.getLanguage()
}