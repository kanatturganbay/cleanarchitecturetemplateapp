package thousand.group.data.repositories.user_info

import thousand.group.data.db.utils.DataBaseUtil
import thousand.group.data.entities.db.UserInfoEntity
import thousand.group.data.entities.remote.UserInfoResponse
import thousand.group.data.remote.utils.RemoteService
import thousand.group.data.storage.utils.LocaleStorage

class UserInfoRepositoryImpl(
    private val remoteService: RemoteService,
    private val dbUtil: DataBaseUtil,
    private val storage: LocaleStorage
) : UserInfoRepository {

    override suspend fun getUser(login: String): UserInfoResponse = remoteService.getUser(login)

    override suspend fun addUserInfo(userInfoModel: UserInfoEntity) =
        dbUtil.getDataDao().addUserInfo(userInfoModel)

    override suspend fun getUserInfo(id: Long): MutableList<UserInfoEntity> =
        dbUtil.getDataDao().getUserInfo(id)
}