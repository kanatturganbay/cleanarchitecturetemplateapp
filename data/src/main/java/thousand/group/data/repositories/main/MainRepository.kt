package thousand.group.data.repositories.main

interface MainRepository {
    fun setLanguage(lang: String)

    fun getLanguage(): String
}