package thousand.group.data.repositories.user_info

import thousand.group.data.entities.db.UserInfoEntity
import thousand.group.data.entities.remote.UserInfoResponse

interface UserInfoRepository {
    suspend fun getUser(login: String): UserInfoResponse

    suspend fun addUserInfo(userInfoModel: UserInfoEntity)

    suspend fun getUserInfo(id: Long): MutableList<UserInfoEntity>
}