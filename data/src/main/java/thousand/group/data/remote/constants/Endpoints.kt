package thousand.group.data.remote.constants

object Endpoints {
    const val register = "user"
    const val getUser = "user/{login}"
}