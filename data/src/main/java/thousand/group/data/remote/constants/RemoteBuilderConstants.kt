package thousand.group.data.remote.constants

object RemoteBuilderConstants {
    const val SERVER_URL = "https://petstore.swagger.io/v2/"
}