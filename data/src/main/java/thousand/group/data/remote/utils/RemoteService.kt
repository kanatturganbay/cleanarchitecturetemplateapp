package thousand.group.data.remote.utils

import com.google.gson.JsonObject
import retrofit2.http.Body
import retrofit2.http.GET
import retrofit2.http.POST
import retrofit2.http.Path
import thousand.group.data.entities.remote.UserInfoResponse
import thousand.group.data.remote.constants.Endpoints

interface RemoteService {

    @POST(Endpoints.register)
    suspend fun register(@Body userData: JsonObject): JsonObject

    @GET(Endpoints.getUser)
    suspend fun getUser(@Path("login") login: String): UserInfoResponse
}