package thousand.group.domain.usecases.registration

import com.google.gson.JsonObject
import thousand.group.domain.response.Response

interface RegistrationUsecase {
    suspend fun register(userData: JsonObject, response: Response<JsonObject>)
}