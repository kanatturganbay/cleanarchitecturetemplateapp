package thousand.group.domain.usecases.registration

import com.google.gson.JsonObject
import thousand.group.data.repositories.registration.RegistrationRepository
import thousand.group.domain.exception.ResponseErrorHandler
import thousand.group.domain.mappers.registration.RegistrationMapper
import thousand.group.domain.response.Response

class RegistrationUsecaseImpl(
    private val mapper: RegistrationMapper,
    private val repository: RegistrationRepository,
    private val errorHandler: ResponseErrorHandler
) : RegistrationUsecase {
    override suspend fun register(userData: JsonObject, response: Response<JsonObject>) {
        try {
            val result = repository.register(userData)
            response.onSuccess(result)
        } catch (ex: Exception) {
            response.onError(errorHandler.traceErrorException(ex))
        }
    }

}