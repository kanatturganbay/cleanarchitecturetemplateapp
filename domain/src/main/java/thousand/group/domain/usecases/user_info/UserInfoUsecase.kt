package thousand.group.domain.usecases.user_info

import thousand.group.domain.entities.UserInfoModel
import thousand.group.domain.response.Response

interface UserInfoUsecase {
    suspend fun getUser(login: String, response: Response<UserInfoModel>)

    suspend fun addUserInfo(userInfoModel: UserInfoModel, response: Response<Boolean>)

    suspend fun getUserInfo(id: Long, response: Response<MutableList<UserInfoModel>>)
}