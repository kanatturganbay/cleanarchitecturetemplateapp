package thousand.group.domain.usecases.main

interface MainUsecase {
    fun setLanguage(lang: String)

    fun getLanguage(): String
}