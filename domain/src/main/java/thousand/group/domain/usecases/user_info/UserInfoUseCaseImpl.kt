package thousand.group.domain.usecases.user_info

import thousand.group.data.repositories.user_info.UserInfoRepository
import thousand.group.domain.entities.UserInfoModel
import thousand.group.domain.exception.ResponseErrorHandler
import thousand.group.domain.mappers.user_info.UserInfoMapper
import thousand.group.domain.response.Response

class UserInfoUseCaseImpl(
    private val mapper: UserInfoMapper,
    private val repository: UserInfoRepository,
    private val errorHandler: ResponseErrorHandler
) : UserInfoUsecase {

    override suspend fun getUser(login: String, response: Response<UserInfoModel>) {
        try {
            val userResponse = repository.getUser(login)
            val mappedResult = mapper.mapGetUser(userResponse)

            response.onSuccess(mappedResult)
        } catch (ex: Exception) {
            response.onError(errorHandler.traceErrorException(ex))
        }
    }

    override suspend fun addUserInfo(
        userInfoModel: UserInfoModel,
        response: Response<Boolean>
    ) {
        try {
            val mappedResult = mapper.mapAddUserInfo(userInfoModel)

            repository.addUserInfo(mappedResult)
            response.onSuccess(true)
        } catch (ex: Exception) {
            response.onError(errorHandler.traceErrorException(ex))
        }
    }

    override suspend fun getUserInfo(id: Long, response: Response<MutableList<UserInfoModel>>) {
        try {
            val userInfoEntityList = repository.getUserInfo(id)
            val mappedResult = mapper.mapGetUserInfo(userInfoEntityList)

            response.onSuccess(mappedResult)
        } catch (ex: Exception) {
            response.onError(errorHandler.traceErrorException(ex))
        }
    }

}