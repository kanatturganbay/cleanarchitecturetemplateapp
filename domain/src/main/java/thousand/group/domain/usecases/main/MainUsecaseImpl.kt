package thousand.group.domain.usecases.main

import thousand.group.data.repositories.main.MainRepository
import thousand.group.domain.exception.ResponseErrorHandler
import thousand.group.domain.mappers.main.MainMapper

class MainUsecaseImpl(
    private val mapper: MainMapper,
    private val repository: MainRepository,
    private val errorHandler: ResponseErrorHandler
) : MainUsecase {

    override fun setLanguage(lang: String) = repository.setLanguage(lang)

    override fun getLanguage(): String = repository.getLanguage()

}