package thousand.group.domain.di

import org.koin.dsl.module
import thousand.group.domain.usecases.main.MainUsecase
import thousand.group.domain.usecases.main.MainUsecaseImpl
import thousand.group.domain.usecases.registration.RegistrationUsecase
import thousand.group.domain.usecases.registration.RegistrationUsecaseImpl
import thousand.group.domain.usecases.user_info.UserInfoUseCaseImpl
import thousand.group.domain.usecases.user_info.UserInfoUsecase

val usecaseModule = module {
    single<RegistrationUsecase> { RegistrationUsecaseImpl(get(), get(), get()) }
    single<UserInfoUsecase> { UserInfoUseCaseImpl(get(), get(), get()) }
    single<MainUsecase> { MainUsecaseImpl(get(), get(), get()) }
}