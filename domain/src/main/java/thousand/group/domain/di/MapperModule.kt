package thousand.group.domain.di

import org.koin.dsl.module
import thousand.group.domain.mappers.main.MainMapper
import thousand.group.domain.mappers.main.MainMapperImpl
import thousand.group.domain.mappers.registration.RegistrationMapper
import thousand.group.domain.mappers.registration.RegistrationMapperImpl
import thousand.group.domain.mappers.user_info.UserInfoMapper
import thousand.group.domain.mappers.user_info.UserInfoMapperImpl

val mapperModule = module {
    single<RegistrationMapper> { RegistrationMapperImpl() }
    single<UserInfoMapper> { UserInfoMapperImpl() }
    single<MainMapper> { MainMapperImpl() }
}