package thousand.group.domain.di

import org.koin.dsl.module
import thousand.group.domain.exception.ResponseError
import thousand.group.domain.exception.ResponseErrorHandler

val domainUtilModule = module {
    single { ResponseErrorHandler }
}