package thousand.group.domain.exception

import thousand.group.domain.R

enum class ErrorStatus(val messageRes: Int) {
    /**
     * Any case where a parameter is invalid, or a required parameter is missing.
     * This includes the case where no OAuth token is provided and
     * the case where a resource ID is specified incorrectly in a path.
     */
    BAD_REQUEST(R.string.error_message_bad_request),

    /**
     * The OAuth token was provided but was invalid.
     */
    UNAUTHORIZED(R.string.error_message_unauthorized),

    /**
     * The requested information cannot be viewed by the acting user, for example,
     * because they are not friends with the user whose data they are trying to read.
     * It could also indicate privileges or access has been revoked.
     */
    FORBIDDEN(R.string.error_message_forbidden),

    /**
     * Endpoint does not exist.
     */
    NOT_FOUND(R.string.error_message_not_found),

    /**
     * Attempting to use POST with a GET-only endpoint, or vice-versa.
     */
    METHOD_NOT_ALLOWED(R.string.error_message_method_not_allowed),

    /**
     * The request could not be completed as it is. Use the information included in the response to modify the request and retry.
     */
    CONFLICT(R.string.error_message_conflict),

    /**
     * There is either a bug on our side or there is an outage.
     * The request is probably valid but needs to be retried later.
     */
    INTERNAL_SERVER_ERROR(R.string.error_message_internal_server),

    /**
     * Time out  error
     */
    TIMEOUT(R.string.error_message_timeout),

    /**
     * Error in connecting to repository (Server or Database)
     */
    NO_CONNECTION(R.string.error_message_no_connection),

    /**
     * When error is not known
     */
    UNKNOWN_ERROR(R.string.error_message_unknown)
}