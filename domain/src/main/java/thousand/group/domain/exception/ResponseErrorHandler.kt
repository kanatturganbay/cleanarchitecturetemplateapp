package thousand.group.domain.exception

import retrofit2.HttpException
import java.io.IOException
import java.net.SocketTimeoutException

/**
 * Trace exceptions(api call or parse data or connection errors) &
 * depending on what exception returns [ApiErrorf]
 *
 * */

object ResponseErrorHandler {
    fun traceErrorException(throwable: Throwable?): ResponseError {

        return when (throwable) {

            is HttpException -> {
                when (throwable.code()) {
                    400 -> ResponseError(
                        throwable.message(),
                        throwable.code(),
                        ErrorStatus.BAD_REQUEST
                    )
                    401 -> ResponseError(
                        throwable.message(),
                        throwable.code(),
                        ErrorStatus.UNAUTHORIZED
                    )
                    403 -> ResponseError(
                        throwable.message(),
                        throwable.code(),
                        ErrorStatus.FORBIDDEN
                    )
                    404 -> ResponseError(
                        throwable.message(),
                        throwable.code(),
                        ErrorStatus.NOT_FOUND
                    )
                    405 -> ResponseError(
                        throwable.message(),
                        throwable.code(),
                        ErrorStatus.METHOD_NOT_ALLOWED
                    )
                    409 -> ResponseError(
                        throwable.message(),
                        throwable.code(),
                        ErrorStatus.CONFLICT
                    )
                    500 -> ResponseError(
                        throwable.message(),
                        throwable.code(),
                        ErrorStatus.INTERNAL_SERVER_ERROR
                    )
                    else -> ResponseError(
                        null,
                        0,
                        ErrorStatus.UNKNOWN_ERROR
                    )
                }
            }

            is SocketTimeoutException -> {
                ResponseError(throwable.message, ErrorStatus.TIMEOUT)
            }

            is IOException -> {
                ResponseError(throwable.message, ErrorStatus.NO_CONNECTION)
            }

            else -> ResponseError(null, 0, ErrorStatus.UNKNOWN_ERROR)
        }
    }
}
