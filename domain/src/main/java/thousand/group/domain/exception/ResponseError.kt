package thousand.group.domain.exception

data class ResponseError(val message: String?, val code: Int?, var errorStatus: ErrorStatus) {
    constructor(message: String?, errorStatus: ErrorStatus) : this(message, null, errorStatus)

    fun getErrorMessage(): Int {
        return errorStatus.messageRes
    }
}