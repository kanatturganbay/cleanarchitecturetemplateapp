package thousand.group.domain.response

import thousand.group.domain.exception.ResponseError

interface Response<Type> {
    fun onSuccess(response: Type)

    fun onError(error: ResponseError)
}