package thousand.group.domain.mappers.user_info

import thousand.group.data.entities.db.UserInfoEntity
import thousand.group.data.entities.remote.UserInfoResponse
import thousand.group.domain.entities.UserInfoModel

class UserInfoMapperImpl : UserInfoMapper {
    override fun mapGetUser(userInfoResponse: UserInfoResponse): UserInfoModel = UserInfoModel(
        userInfoResponse.id,
        userInfoResponse.username,
        userInfoResponse.firstName,
        userInfoResponse.lastName,
        userInfoResponse.email,
        userInfoResponse.password,
        userInfoResponse.phone,
        userInfoResponse.userStatus
    )

    override fun mapAddUserInfo(userInfoModel: UserInfoModel): UserInfoEntity = UserInfoEntity(
        id = userInfoModel.id,
        username = userInfoModel.username,
        firstName = userInfoModel.firstName,
        lastName = userInfoModel.lastName,
        email = userInfoModel.email,
        password = userInfoModel.password,
        phone = userInfoModel.phone,
        userStatus = userInfoModel.userStatus
    )

    override fun mapGetUserInfo(userInfoEntity: MutableList<UserInfoEntity>): MutableList<UserInfoModel> {
        val mutableList = mutableListOf<UserInfoModel>()

        userInfoEntity.forEach {
            mutableList.add(
                UserInfoModel(
                    it.id,
                    it.username,
                    it.firstName,
                    it.lastName,
                    it.email,
                    it.password,
                    it.phone,
                    it.userStatus
                )
            )
        }

        return mutableList
    }

}