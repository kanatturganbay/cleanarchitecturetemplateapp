package thousand.group.domain.mappers.user_info

import thousand.group.data.entities.db.UserInfoEntity
import thousand.group.data.entities.remote.UserInfoResponse
import thousand.group.domain.entities.UserInfoModel

interface UserInfoMapper {

    fun mapGetUser(userInfoResponse: UserInfoResponse): UserInfoModel

    fun mapAddUserInfo(userInfoModel: UserInfoModel): UserInfoEntity

    fun mapGetUserInfo(userInfoEntity: MutableList<UserInfoEntity>): MutableList<UserInfoModel>

}