package thousand.group.examplecleanarchitecture.app

import android.app.Application
import org.koin.android.ext.koin.androidContext
import org.koin.core.KoinComponent
import org.koin.core.context.startKoin
import thousand.group.data.storage.utils.CreateLocaleStoragePrefs
import thousand.group.examplecleanarchitecture.utils.di.appModule

class ClearApp : Application(), KoinComponent {
    override fun onCreate() {
        super.onCreate()

        CreateLocaleStoragePrefs.initLocalePrefs(this)

        startKoin {
            androidContext(this@ClearApp)
            modules(appModule)
        }
    }

}