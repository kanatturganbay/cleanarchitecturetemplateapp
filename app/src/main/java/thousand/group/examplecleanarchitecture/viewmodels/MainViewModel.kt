package thousand.group.examplecleanarchitecture.viewmodels

import thousand.group.domain.usecases.main.MainUsecase
import thousand.group.examplecleanarchitecture.utils.base.BaseViewModel
import thousand.group.examplecleanarchitecture.utils.base.SharedViewModel
import thousand.group.examplecleanarchitecture.utils.extensions.call
import thousand.group.examplecleanarchitecture.utils.models.LangMode
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer
import thousand.group.examplecleanarchitecture.utils.system.OnceMutableLiveData
import thousand.group.examplecleanarchitecture.utils.system.ResourceManager
import thousand.group.testknp.global.simple.AppConstants

class MainViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    private val usecase: MainUsecase
) : BaseViewModel(resourceManager, sharedViewModel) {

    init {
        setStringMessageReceivedListener { key, message ->
            showMessageToast("key:$key, message: $message")
        }
        setIntMessageReceivedListener { key, message ->
            showMessageToast("key:$key, message: $message")

        }
        setBooleanMessageReceivedListener { key, message ->
            showMessageToast("key:$key, message: $message")

        }
        setSerializableMessageReceivedListener { key, message ->
            showMessageToast("key:$key, message: $message")

        }
        setParcelableMessageReceivedListener { key, message ->
            showMessageToast("key:$key, message: $message")

        }
        setUnitMessageReceivedListener {
            showMessageToast("key:$it")
        }
    }

    val ldRestartActivity = OnceMutableLiveData<Unit>()
    val ldTestSimpleCallback = OnceMutableLiveData<Unit>()
    val ldOpenTestParamsWithParamsContainer = OnceMutableLiveData<ParamsContainer>()
    val ldOpenTestParamsWithParams = OnceMutableLiveData<Triple<Int, String, LangMode>>()

    private val langCodes = mutableListOf(
        LangMode("ru", "ru"),
        LangMode("en", "en")
    )

    fun changeLanguage() {
        if (usecase.getLanguage().equals(langCodes.get(0).langCodeLocale)) {
            usecase.setLanguage(langCodes.get(1).langCodeLocale)
        } else {
            usecase.setLanguage(langCodes.get(0).langCodeLocale)
        }
        ldRestartActivity.call()
    }

    fun testLocaleStorage() {
        showMessageToast(usecase.getLanguage())
    }

    fun openTestParamsWithParamsContainer() {
        val paramsContainer = ParamsContainer()

        paramsContainer.putInt(AppConstants.BundleConstants.params1, 1)
        paramsContainer.putString(AppConstants.BundleConstants.params2, "Kanat")
        paramsContainer.putParcelable(AppConstants.BundleConstants.params3, langCodes.get(0))

        ldOpenTestParamsWithParamsContainer.postValue(paramsContainer)
    }

    fun openTestParamsWithParams() {
        val params = Triple(1, "Kanat", langCodes.get(0))
        ldOpenTestParamsWithParams.postValue(params)
    }

    fun testSingleCallback() {
        ldTestSimpleCallback.call()
    }

}