package thousand.group.examplecleanarchitecture.viewmodels

import thousand.group.domain.entities.UserInfoModel
import thousand.group.domain.exception.ResponseError
import thousand.group.domain.response.Response
import thousand.group.domain.usecases.user_info.UserInfoUsecase
import thousand.group.examplecleanarchitecture.R
import thousand.group.examplecleanarchitecture.utils.base.BaseViewModel
import thousand.group.examplecleanarchitecture.utils.base.SharedViewModel
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer
import thousand.group.examplecleanarchitecture.utils.system.OnceMutableLiveData
import thousand.group.examplecleanarchitecture.utils.system.ResourceManager

class UserInfoViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val usecase: UserInfoUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetText = OnceMutableLiveData<String>()

    fun getUserInfo(
        login_edit: String
    ) {

        when {
            login_edit.trim().isEmpty() -> {
                showMessageError(R.string.error_login)
                return
            }
        }

        doWork {
            usecase.getUser(login_edit, object : Response<UserInfoModel> {
                override fun onSuccess(response: UserInfoModel) {
                    addUserDatabase(response)
                }

                override fun onError(error: ResponseError) {
                    showMessageError(error.getErrorMessage())
                }
            })
        }
    }

    private fun addUserDatabase(user: UserInfoModel) {
        doWork {
            usecase.addUserInfo(user, object : Response<Boolean> {
                override fun onSuccess(response: Boolean) {
                    getUserAndSetText(user.id)
                }

                override fun onError(error: ResponseError) {
                    showMessageError(error.getErrorMessage())
                }
            })
        }
    }

    private fun getUserAndSetText(user_id: Long) {
        doWork {
            usecase.getUserInfo(user_id, object : Response<MutableList<UserInfoModel>> {
                override fun onSuccess(response: MutableList<UserInfoModel>) {
                    ldSetText.postValue(response.toString())
                }

                override fun onError(error: ResponseError) {
                }

            })
        }
    }
}