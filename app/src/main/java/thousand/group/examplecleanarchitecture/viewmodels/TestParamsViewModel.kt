package thousand.group.examplecleanarchitecture.viewmodels

import thousand.group.examplecleanarchitecture.utils.base.BaseViewModel
import thousand.group.examplecleanarchitecture.utils.base.SharedViewModel
import thousand.group.examplecleanarchitecture.utils.models.LangMode
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer
import thousand.group.examplecleanarchitecture.utils.system.OnceMutableLiveData
import thousand.group.examplecleanarchitecture.utils.system.ResourceManager
import thousand.group.testknp.global.simple.AppConstants

class TestParamsViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetParams = OnceMutableLiveData<String>()

    private val slashN = "\n"

    fun parseParams() {
        paramsContainer?.apply {
            val sb = StringBuilder()

            sb
                .append(
                    getInt(AppConstants.BundleConstants.params1)
                )
                .append(slashN)
                .append(
                    getString(AppConstants.BundleConstants.params2)
                )
                .append(slashN)
                .append(
                    getParcelable(AppConstants.BundleConstants.params3) as LangMode
                )
                .append(slashN)

            ldSetParams.postValue(sb.toString())
        }
    }
}