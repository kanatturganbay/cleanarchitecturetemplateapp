package thousand.group.examplecleanarchitecture.viewmodels

import com.google.gson.JsonObject
import thousand.group.domain.exception.ResponseError
import thousand.group.domain.response.Response
import thousand.group.domain.usecases.registration.RegistrationUsecase
import thousand.group.examplecleanarchitecture.R
import thousand.group.examplecleanarchitecture.utils.base.BaseViewModel
import thousand.group.examplecleanarchitecture.utils.base.SharedViewModel
import thousand.group.examplecleanarchitecture.utils.extensions.call
import thousand.group.examplecleanarchitecture.utils.extensions.isEmailValid
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer
import thousand.group.examplecleanarchitecture.utils.system.OnceMutableLiveData
import thousand.group.examplecleanarchitecture.utils.system.ResourceManager
import thousand.group.testknp.global.simple.AppConstants

class RegistrationViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?,
    private val usecase: RegistrationUsecase
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    val ldSetData = OnceMutableLiveData<Unit>()

    fun regBtnClicked(
        login_edit: String,
        name_edit: String,
        surname_edit: String,
        email_edit: String,
        phone_edit: String,
        password_edit: String,
        status_edit: String
    ) {

        when {
            !email_edit.isEmailValid() -> {
                showMessageToast(R.string.error_email)
                return
            }
            phone_edit.trim().length != 11 -> {
                showMessageToast(R.string.error_phone)
                return
            }
        }

        val jsonObject = JsonObject()

        jsonObject.addProperty(AppConstants.ApiFieldConstants.id, 0)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.username, login_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.firstName, name_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.lastName, surname_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.email, email_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.password, password_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.phone, phone_edit)
        jsonObject.addProperty(AppConstants.ApiFieldConstants.userStatus, status_edit)

        doWork{
            usecase.register(jsonObject, object: Response<JsonObject>{
                override fun onSuccess(response: JsonObject) {
                    showMessageSuccess(response.toString())
                    ldSetData.call()
                }

                override fun onError(error: ResponseError) {
                    showMessageError(error.getErrorMessage())
                }

            })
        }
    }

}