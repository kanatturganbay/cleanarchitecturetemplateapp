package thousand.group.examplecleanarchitecture.viewmodels

import thousand.group.examplecleanarchitecture.utils.base.BaseViewModel
import thousand.group.examplecleanarchitecture.utils.base.SharedViewModel
import thousand.group.examplecleanarchitecture.utils.models.LangMode
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer
import thousand.group.examplecleanarchitecture.utils.system.ResourceManager
import thousand.group.testknp.global.simple.AppConstants

class TestConnectorsViewModel(
    override val resourceManager: ResourceManager,
    override val sharedViewModel: SharedViewModel,
    override var paramsContainer: ParamsContainer?
) : BaseViewModel(resourceManager, sharedViewModel, paramsContainer) {

    fun sendParcelable() {
        sendLocaleMessage(
            AppConstants.SharedActionConstants.SEND_PARCELABLE,
            LangMode("", "", false)
        )
    }

    fun sendSerializable() {
        sendLocaleMessage(
            AppConstants.SharedActionConstants.SEND_SERIALIZABLE,
            ParamsContainer()
        )
    }

    fun sendCallback() {
        sendLocaleCallback(
            AppConstants.SharedActionConstants.SEND_CALLBACK
        )
    }

    fun sendString() {
        sendLocaleMessage(
            AppConstants.SharedActionConstants.SEND_STRING,
            AppConstants.SharedActionConstants.SEND_STRING
        )
    }

    fun sendInt() {
        sendLocaleMessage(
            AppConstants.SharedActionConstants.SEND_INT,
            AppConstants.SharedActionConstants.SEND_INT
        )
    }

    fun sendBoolean() {
        sendLocaleMessage(
            AppConstants.SharedActionConstants.SEND_BOOLEAN,
            AppConstants.SharedActionConstants.SEND_BOOLEAN
        )
    }

}