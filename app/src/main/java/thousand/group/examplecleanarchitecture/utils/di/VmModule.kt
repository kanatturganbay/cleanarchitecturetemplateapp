package thousand.group.examplecleanarchitecture.utils.di

import android.content.Context
import org.koin.androidx.viewmodel.dsl.viewModel
import org.koin.core.parameter.parametersOf
import org.koin.dsl.module
import thousand.group.examplecleanarchitecture.utils.base.SharedViewModel
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer
import thousand.group.examplecleanarchitecture.viewmodels.*

val vmModule = module {
    viewModel { SharedViewModel() }

    viewModel { (context: Context, sharedViewModel: SharedViewModel) ->
        MainViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        RegistrationViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        UserInfoViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer,
            get()
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        TestParamsViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer
        )
    }

    viewModel { (context: Context, sharedViewModel: SharedViewModel, paramsContainer: ParamsContainer) ->
        TestConnectorsViewModel(
            get {
                parametersOf(
                    context
                )
            },
            sharedViewModel,
            paramsContainer
        )
    }

}