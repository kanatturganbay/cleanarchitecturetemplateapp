package thousand.group.examplecleanarchitecture.utils.helpers

import android.annotation.SuppressLint
import android.annotation.TargetApi
import android.content.Context
import android.content.ContextWrapper
import android.content.res.Configuration
import android.os.Build
import android.util.Log
import thousand.group.data.storage.utils.LocaleStorage
import java.util.*

class LocaleHelper(val base: Context, val storage: LocaleStorage) {

    @SuppressLint("ObsoleteSdkInt")
    fun wrap(): ContextWrapper {
        var contextClone = base
        try {
            val config = contextClone.resources.configuration

            val locale = Locale(storage.getLanguage())

            Log.i("LocaleHelper", storage.getLanguage())

            Locale.setDefault(locale)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                setSystemLocale(config, locale)
            } else {
                setSystemLocaleLegacy(config, locale)
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.JELLY_BEAN_MR1) {
                config.setLayoutDirection(locale)
                contextClone = contextClone.createConfigurationContext(config)
            } else {
                contextClone.resources.updateConfiguration(
                    config,
                    contextClone.resources.displayMetrics
                )
            }
        } catch (e: ClassCastException) {
            e.printStackTrace()
        }

        return ContextWrapper(contextClone)
    }

    fun getSystemLocaleLegacy(config: Configuration): Locale {
        return config.locale
    }

    @TargetApi(Build.VERSION_CODES.N)
    fun getSystemLocale(config: Configuration): Locale {
        return config.locales.get(0)
    }

    fun setSystemLocaleLegacy(config: Configuration, locale: Locale) {
        config.locale = locale
    }

    @TargetApi(Build.VERSION_CODES.N)
    fun setSystemLocale(config: Configuration, locale: Locale) {
        config.setLocale(locale)
    }
}