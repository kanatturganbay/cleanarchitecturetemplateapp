package thousand.group.examplecleanarchitecture.utils.models

import android.os.Parcelable
import kotlinx.android.parcel.Parcelize

@Parcelize
data class LangMode(
    var langCodeServer: String,
    var langCodeLocale: String,
    var isSelected: Boolean = false
) : Parcelable