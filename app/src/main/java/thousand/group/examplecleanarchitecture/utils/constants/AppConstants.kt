package thousand.group.testknp.global.simple

object AppConstants {
    object StatusCodeConstants {
        const val STATUS_CODE_200 = 200
    }

    object ApiFieldConstants {
        const val id = "id"
        const val username = "username"
        const val firstName = "firstName"
        const val lastName = "lastName"
        const val email = "email"
        const val password = "password"
        const val phone = "phone"
        const val userStatus = "userStatus"
    }

    object BundleConstants {
        const val PARAMS_CONTAINER = "PARAMS_CONTAINER"
        const val params1 = "PARAMS_1"
        const val params2 = "PARAMS_2"
        const val params3 = "PARAMS_3"
    }

    object SharedActionConstants{
        const val SEND_PARCELABLE = "SEND_PARCELABLE"
        const val SEND_SERIALIZABLE = "SEND_SERIALIZABLE"
        const val SEND_CALLBACK = "SEND_CALLBACK"
        const val SEND_STRING = "SEND_STRING"
        const val SEND_INT = "SEND_INT"
        const val SEND_BOOLEAN = "SEND_BOOLEAN"
    }
}