package thousand.group.examplecleanarchitecture.utils.extensions

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LiveData
import androidx.lifecycle.MediatorLiveData
import androidx.lifecycle.Observer
import thousand.group.examplecleanarchitecture.utils.system.OnceMutableLiveData

inline fun <T> LifecycleOwner.observeLiveData(
    data: LiveData<T>,
    crossinline onChanged: (T) -> Unit
) {
    data.removeObservers(this)
    data.observe(this, Observer {
        it?.let { value -> onChanged(value) }
    })
}

inline fun <reified T> OnceMutableLiveData<T>.call() {
    when (T::class) {
        Unit::class -> {
            postValue(Unit as T)
        }
    }
}

fun <A, B> zipLiveData(a: LiveData<A>, b: LiveData<B>): LiveData<Pair<A, B>> {
    return MediatorLiveData<Pair<A, B>>().apply {
        var lastA: A? = null
        var lastB: B? = null

        fun update() {
            val localLastA = lastA
            val localLastB = lastB

            if (localLastA != null && localLastB != null) {
                this.value = Pair(localLastA, localLastB)
            }
        }

        addSource(a) {
            lastA = it
            update()
        }
        addSource(b) {
            lastB = it
            update()
        }
    }
}

