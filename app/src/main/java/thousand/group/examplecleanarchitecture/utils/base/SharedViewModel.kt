package thousand.group.examplecleanarchitecture.utils.base

import android.os.Parcelable
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import thousand.group.examplecleanarchitecture.utils.extensions.call
import thousand.group.examplecleanarchitecture.utils.system.OnceMutableLiveData
import java.io.Serializable

class SharedViewModel : ViewModel() {

    val ldSuccessStr = OnceMutableLiveData<String>()
    val ldSuccessInt = OnceMutableLiveData<Int>()
    val ldErrorStr = OnceMutableLiveData<String>()
    val ldErrorInt = OnceMutableLiveData<Int>()
    val ldProgressShow = OnceMutableLiveData<Unit>()
    val ldProgressHide = OnceMutableLiveData<Unit>()
    val ldToastStr = OnceMutableLiveData<String>()
    val ldToastInt = OnceMutableLiveData<Int>()
    val ldOpenKeyboard = OnceMutableLiveData<Unit>()
    val ldCloseKeyboard = OnceMutableLiveData<Unit>()

    val ldSendMessageInt = OnceMutableLiveData<Pair<String, Int>>()
    val ldSendMessageStr = OnceMutableLiveData<Pair<String, String>>()
    val ldSendMessageParcelable = OnceMutableLiveData<Pair<String, Parcelable>>()
    val ldSendMessageBoolean = OnceMutableLiveData<Pair<String, Boolean>>()
    val ldSendMessageSerializable = OnceMutableLiveData<Pair<String, Serializable>>()
    val ldSendMessageCallback = OnceMutableLiveData<String>()

    fun showMessageSuccess(message: String) = ldSuccessStr.postValue(message)

    fun showMessageSuccess(@StringRes message: Int) = ldSuccessInt.postValue(message)

    fun showMessageError(message: String) = ldErrorStr.postValue(message)

    fun showMessageError(@StringRes message: Int) = ldErrorInt.postValue(message)

    fun showMessageToast(message: String) = ldToastStr.postValue(message)

    fun showMessageToast(@StringRes message: Int) = ldToastInt.postValue(message)

    fun showProgressBar(show: Boolean) {
        if (show) {
            ldProgressShow.call()
        } else {
            ldProgressHide.call()
        }
    }

    fun openKeyBoard() = ldOpenKeyboard.call()

    fun closeKeyBoard() = ldCloseKeyboard.call()

    fun <T> sendLocaleMessage(key: String, message: T?) {
        when (message) {
            is Parcelable -> {
                ldSendMessageParcelable.postValue(Pair(key, message))
            }
            is String -> {
                ldSendMessageStr.postValue(Pair(key, message))
            }
            is Boolean -> {
                ldSendMessageBoolean.postValue(Pair(key, message))
            }
            is Int -> {
                ldSendMessageInt.postValue(Pair(key, message))
            }
            is Serializable -> {
                ldSendMessageSerializable.postValue(Pair(key, message))
            }
            null -> {
                ldSendMessageCallback.postValue(key)
            }
        }
    }

}