package thousand.group.examplecleanarchitecture.utils.di

import thousand.group.data.di.dbModule
import thousand.group.data.di.remoteModule
import thousand.group.data.di.repModule
import thousand.group.data.di.storageModule
import thousand.group.domain.di.domainUtilModule
import thousand.group.domain.di.mapperModule
import thousand.group.domain.di.usecaseModule

val appModule = listOf(
    dbModule,
    remoteModule,
    storageModule,
    repModule,
    domainUtilModule,
    mapperModule,
    usecaseModule,
    appUtilModule,
    vmModule
)