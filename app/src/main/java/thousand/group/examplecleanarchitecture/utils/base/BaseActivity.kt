package thousand.group.examplecleanarchitecture.utils.base

import android.content.Context
import android.content.Intent
import android.content.res.Configuration
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.util.Log
import android.view.MotionEvent
import android.view.View
import android.view.WindowManager
import android.view.inputmethod.InputMethodManager
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import org.koin.android.ext.android.inject
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import thousand.group.examplecleanarchitecture.R
import thousand.group.examplecleanarchitecture.utils.extensions.observeLiveData
import thousand.group.examplecleanarchitecture.utils.helpers.LocaleHelper
import thousand.group.examplecleanarchitecture.utils.helpers.MessageStatusHelper
import thousand.group.examplecleanarchitecture.utils.helpers.ProgressBarHelper
import kotlin.reflect.KClass

abstract class BaseActivity<out ViewModelType : BaseViewModel>(clazz: KClass<ViewModelType>) :
    AppCompatActivity() {

    var activityTag: String = this.javaClass.simpleName

    abstract var layoutResId: Int

    private var startFlag = false

    protected val sharedViewModel: SharedViewModel by viewModel()

    protected val viewModel: ViewModelType by viewModel(clazz) {
        parametersOf(this, sharedViewModel)
    }

    private val messageHelper: MessageStatusHelper by inject {
        parametersOf(this)
    }
    private val progressHelper: ProgressBarHelper by inject {
        parametersOf(this)
    }

    private var connManager: ConnectivityManager? = null
    private var internetAccess = false

    private val networkRequest = NetworkRequest.Builder()
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .build()

    private val networkListener = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            internetSuccess()
            internetAccess = true
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            internetError()
            internetAccess = false
        }

        override fun onUnavailable() {
            super.onUnavailable()
            internetError()
            internetAccess = false
        }
    }

    override fun dispatchTouchEvent(ev: MotionEvent?): Boolean {
        if (currentFocus != null) {
            if (startFlag) {
                val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
                imm.hideSoftInputFromWindow(currentFocus!!.windowToken, 0)
            } else {
                startFlag = true
            }
        }
        return super.dispatchTouchEvent(ev)
    }

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layoutResId)

        setStandartLiveDataCallback()
        addConnectionCallback()
        registerFragmentLifecycle()

        initIntent(intent)
        initView(savedInstanceState)
        initLiveData()
        initController()

        startFlag = true

    }

    override fun onDestroy() {
        viewModelStore.clear()
        super.onDestroy()
    }

    override fun attachBaseContext(newBase: Context?) {
        newBase?.apply {
            try {
                val localeHelper: LocaleHelper by inject {
                    parametersOf(this)
                }

                super.attachBaseContext(localeHelper.wrap())
            } catch (e: ClassCastException) {
                e.printStackTrace()
            }
        }
    }

    override fun applyOverrideConfiguration(overrideConfiguration: Configuration?) {

        overrideConfiguration?.let {
            val uiMode = it.uiMode
            it.setTo(baseContext.resources.configuration)
            it.uiMode = uiMode
        }

        super.applyOverrideConfiguration(overrideConfiguration)
    }

    protected fun closeKeyboard() {
        currentFocus?.apply {
            val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
            imm.hideSoftInputFromWindow(windowToken, 0)
        }
    }

    protected fun openKeyboard() {
        val imm = getSystemService(Context.INPUT_METHOD_SERVICE) as InputMethodManager
        imm.toggleSoftInput(InputMethodManager.SHOW_FORCED, 0)
    }

    fun changeStatusBarColor(colorRess: Int) {
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

            val color = ContextCompat.getColor(this, colorRess)

            val colorNav = if (colorRess == R.color.colorWhite) {
                ContextCompat.getColor(this, R.color.colorBlack)
            } else {
                color
            }

            val window = window

            window.addFlags(WindowManager.LayoutParams.FLAG_DRAWS_SYSTEM_BAR_BACKGROUNDS)

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                if (colorRess == R.color.colorWhite) {
                    window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LIGHT_STATUS_BAR
                } else {
                    window.decorView.systemUiVisibility = 0
                }
            }

            window.statusBarColor = color
            window.navigationBarColor = colorNav
        }
    }

    protected fun setStandartLiveDataCallback() {

        viewModel.let {
            observeLiveData(sharedViewModel.ldSuccessStr) {
                messageHelper.showDialogMessageSuccess(it)
            }
            observeLiveData(sharedViewModel.ldSuccessInt) {
                messageHelper.showDialogMessageSuccess(it)
            }
            observeLiveData(sharedViewModel.ldErrorStr) {
                messageHelper.showDialogMessageError(it)
            }
            observeLiveData(sharedViewModel.ldErrorInt) {
                messageHelper.showDialogMessageError(it)
            }
            observeLiveData(sharedViewModel.ldProgressShow) {
                progressHelper.showProgress()
            }
            observeLiveData(sharedViewModel.ldProgressHide) {
                progressHelper.hideProgress()
            }
            observeLiveData(sharedViewModel.ldToastStr) {
                Toast.makeText(this@BaseActivity, it, Toast.LENGTH_SHORT).show()
            }
            observeLiveData(sharedViewModel.ldToastInt) {
                Toast.makeText(this@BaseActivity, it!!, Toast.LENGTH_SHORT).show()
            }
            observeLiveData(sharedViewModel.ldOpenKeyboard) {
                openKeyboard()
            }
            observeLiveData(sharedViewModel.ldCloseKeyboard) {
                closeKeyboard()
            }
            observeLiveData(sharedViewModel.ldSendMessageStr) {
                viewModel.parseReceivedMessage(it.first, it.second)
            }
            observeLiveData(sharedViewModel.ldSendMessageInt) {
                viewModel.parseReceivedMessage(it.first, it.second)
            }
            observeLiveData(sharedViewModel.ldSendMessageBoolean) {
                viewModel.parseReceivedMessage(it.first, it.second)
            }
            observeLiveData(sharedViewModel.ldSendMessageParcelable) {
                viewModel.parseReceivedMessage(it.first, it.second)
            }
            observeLiveData(sharedViewModel.ldSendMessageSerializable) {
                viewModel.parseReceivedMessage(it.first, it.second)
            }
            observeLiveData(sharedViewModel.ldSendMessageCallback) {
                viewModel.parseReceivedMessage(it, null)
            }
        }
    }


    protected fun addConnectionCallback() {
        connManager =
            getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        connManager?.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                registerDefaultNetworkCallback(networkListener)
            } else {
                registerNetworkCallback(
                    networkRequest,
                    networkListener
                )
            }
        }
    }

    private fun registerFragmentLifecycle() {
        supportFragmentManager.registerFragmentLifecycleCallbacks(
            object : FragmentManager.FragmentLifecycleCallbacks() {

                override fun onFragmentStarted(fm: FragmentManager, f: Fragment) {
                    super.onFragmentStarted(fm, f)

                    f.tag?.apply {
                        Log.i(activityTag, this)
                        fragmentLifeCycleController(this)
                    }
                }

                override fun onFragmentPaused(fm: FragmentManager, f: Fragment) {
                    super.onFragmentPaused(fm, f)

                    if (!fm.fragments.isNullOrEmpty()) {
                        val tag = fm.fragments[fm.fragments.size - 1].tag

                        tag?.apply {
                            Log.i(activityTag, this)
                            fragmentLifeCycleController(this)
                        }
                    }
                }

                override fun onFragmentResumed(fm: FragmentManager, f: Fragment) {
                    super.onFragmentResumed(fm, f)

                    if (!fm.fragments.isNullOrEmpty()) {
                        val tag = fm.fragments[fm.fragments.size - 1].tag

                        tag?.apply {
                            Log.i(activityTag, this)
                            fragmentLifeCycleController(this)
                        }
                    }
                }
            },
            true
        )
    }

    abstract fun internetSuccess()

    abstract fun internetError()

    abstract fun initIntent(intent: Intent?)

    abstract fun initView(savedInstanceState: Bundle?)

    abstract fun initLiveData()

    abstract fun initController()

    abstract fun fragmentLifeCycleController(tag: String)

}