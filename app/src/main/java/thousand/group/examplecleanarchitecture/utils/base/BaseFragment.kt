package thousand.group.examplecleanarchitecture.utils.base

import android.content.Context
import android.net.ConnectivityManager
import android.net.Network
import android.net.NetworkCapabilities
import android.net.NetworkRequest
import android.os.Build
import android.os.Bundle
import android.os.Parcelable
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.core.content.ContextCompat.getSystemService
import androidx.fragment.app.Fragment
import org.koin.androidx.viewmodel.ext.android.sharedViewModel
import org.koin.androidx.viewmodel.ext.android.viewModel
import org.koin.core.parameter.parametersOf
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer.Companion.createParamsContainer
import thousand.group.testknp.global.simple.AppConstants
import java.io.Serializable
import kotlin.reflect.KClass

abstract class BaseFragment<out ViewModelType : BaseViewModel>(clazz: KClass<ViewModelType>) :
    Fragment() {

    var fragmentTag: String = this.javaClass.simpleName

    abstract var layoutResId: Int
    abstract var fragmentMainTag: String

    protected val sharedViewModell: SharedViewModel by sharedViewModel()

    protected val viewModel: ViewModelType by viewModel(clazz) {
        parametersOf(requireContext(), sharedViewModell, parseBundle())
    }

    private var connManager: ConnectivityManager? = null
    private var internetAccess = false

    private val networkRequest = NetworkRequest.Builder()
        .addCapability(NetworkCapabilities.NET_CAPABILITY_INTERNET)
        .build()

    private val networkListener = object : ConnectivityManager.NetworkCallback() {
        override fun onAvailable(network: Network) {
            super.onAvailable(network)
            internetSuccess()
            internetAccess = true
        }

        override fun onLost(network: Network) {
            super.onLost(network)
            internetError()
            internetAccess = false
        }

        override fun onUnavailable() {
            super.onUnavailable()
            internetError()
            internetAccess = false
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = inflater.inflate(layoutResId, container, false)

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        addConnectionCallback()

        initView(savedInstanceState)
        initLiveData()
        initController()

    }

    override fun onDestroy() {
        super.onDestroy()

        viewModelStore.clear()
    }

    protected fun addConnectionCallback() {
        connManager =
            requireContext().getSystemService(Context.CONNECTIVITY_SERVICE) as ConnectivityManager

        connManager?.apply {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                registerDefaultNetworkCallback(networkListener)
            } else {
                registerNetworkCallback(
                    networkRequest,
                    networkListener
                )
            }
        }
    }

    private fun parseBundle(): ParamsContainer? {
        var paramsContainer: ParamsContainer? = null

        arguments?.let { bundle ->
            val pc = bundle.getSerializable(AppConstants.BundleConstants.PARAMS_CONTAINER)

            if (pc != null) {
                paramsContainer = pc as ParamsContainer
            } else {
                paramsContainer = createParamsContainer(bundle)
            }
        }

        return paramsContainer
    }

    abstract fun internetSuccess()

    abstract fun internetError()

    abstract fun initView(savedInstanceState: Bundle?)

    abstract fun initLiveData()

    abstract fun initController()
}
