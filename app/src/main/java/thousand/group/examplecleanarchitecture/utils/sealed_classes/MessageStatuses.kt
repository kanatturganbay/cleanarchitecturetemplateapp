package thousand.group.examplecleanarchitecture.utils.kanat

sealed class MessageStatuses {
    internal object ON_SUCCESS : MessageStatuses()
    internal object ON_ERROR : MessageStatuses()
}