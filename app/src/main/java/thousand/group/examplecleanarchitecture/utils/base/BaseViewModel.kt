package thousand.group.examplecleanarchitecture.utils.base

import android.os.Parcelable
import androidx.annotation.StringRes
import androidx.lifecycle.ViewModel
import kotlinx.coroutines.*
import kotlinx.coroutines.Dispatchers.IO
import kotlinx.coroutines.Dispatchers.Main
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer
import thousand.group.examplecleanarchitecture.utils.system.ResourceManager
import java.io.Serializable
import kotlin.coroutines.CoroutineContext

abstract class BaseViewModel(
    open val resourceManager: ResourceManager,
    open val sharedViewModel: SharedViewModel,
    open var paramsContainer: ParamsContainer? = null
) : ViewModel() {

    var vmTag: String = this.javaClass.simpleName

    private var viewModelJob = Job()
    private val viewModelScope = CoroutineScope(Main + viewModelJob)
    private var isActive = true

    private lateinit var stringMessageListener: (key: String, message: String) -> Unit
    private lateinit var intMessageListener: (key: String, message: Int) -> Unit
    private lateinit var booleanMessageListener: (key: String, message: Boolean) -> Unit
    private lateinit var parcelableMessageListener: (key: String, message: Parcelable) -> Unit
    private lateinit var serializableMessageListener: (key: String, message: Serializable) -> Unit
    private lateinit var unitMessageListener: (key: String) -> Unit

    override fun onCleared() {
        super.onCleared()
        isActive = false
        viewModelJob.cancel()
    }

    fun showMessageSuccess(message: String) = sharedViewModel.showMessageSuccess(message)

    fun showMessageSuccess(@StringRes message: Int) = sharedViewModel.showMessageSuccess(message)

    fun showMessageError(message: String) = sharedViewModel.showMessageError(message)

    fun showMessageError(@StringRes message: Int) = sharedViewModel.showMessageError(message)

    fun showMessageToast(message: String) = sharedViewModel.showMessageToast(message)

    fun showMessageToast(@StringRes message: Int) = sharedViewModel.showMessageToast(message)

    fun showProgressBar(show: Boolean) = sharedViewModel.showProgressBar(show)

    fun openKeyBoard() = sharedViewModel.openKeyBoard()

    fun closeKeyBoard() = sharedViewModel.closeKeyBoard()

    fun <T> sendLocaleMessage(key: String, message: T) =
        sharedViewModel.sendLocaleMessage(key, message)

    fun sendLocaleCallback(key: String) = sharedViewModel.sendLocaleMessage(key, null)

    // Do work in IO
    fun <P> doWork(doOnAsyncBlock: suspend CoroutineScope.() -> P) {
        doCoroutineWork(doOnAsyncBlock, viewModelScope, IO)
    }

    // Do work in Main
    // doWorkInMainThread {...}
    fun <P> doWorkInMainThread(doOnAsyncBlock: suspend CoroutineScope.() -> P) {
        doCoroutineWork(doOnAsyncBlock, viewModelScope, Main)
    }

    // Do work in IO repeately
    // doRepeatWork(1000) {...}
    // then we need to stop it calling stopRepeatWork()
    fun <P> doRepeatWork(delayl: Long, doOnAsyncBlock: suspend CoroutineScope.() -> P) {
        isActive = true
        viewModelScope.launch {
            while (this@BaseViewModel.isActive) {
                withContext(IO) {
                    doOnAsyncBlock.invoke(this)
                }
                if (this@BaseViewModel.isActive) {
                    delay(delayl)
                }
            }
        }
    }

    fun stopRepeatWork() {
        isActive = false
    }

    fun <T> parseReceivedMessage(key: String, message: T?) {

        when (message) {
            is String -> {
                if (::stringMessageListener.isInitialized) {
                    stringMessageListener.invoke(key, message)
                }
            }
            is Int -> {
                if (::intMessageListener.isInitialized) {
                    intMessageListener.invoke(key, message)
                }
            }
            is Boolean -> {
                if (::booleanMessageListener.isInitialized) {
                    booleanMessageListener.invoke(key, message)
                }
            }
            is Parcelable -> {
                if (::parcelableMessageListener.isInitialized) {
                    parcelableMessageListener.invoke(key, message)
                }
            }
            is Serializable -> {
                if (::serializableMessageListener.isInitialized) {
                    serializableMessageListener.invoke(key, message)
                }
            }
            else -> {
                if (::unitMessageListener.isInitialized) {
                    unitMessageListener.invoke(key)
                }
            }
        }
    }

    protected fun setStringMessageReceivedListener(listener: (key: String, message: String) -> Unit) {
        this.stringMessageListener = listener
    }

    protected fun setIntMessageReceivedListener(listener: (key: String, message: Int) -> Unit) {
        this.intMessageListener = listener
    }

    protected fun setBooleanMessageReceivedListener(listener: (key: String, message: Boolean) -> Unit) {
        this.booleanMessageListener = listener
    }

    protected fun setParcelableMessageReceivedListener(listener: (key: String, message: Parcelable) -> Unit) {
        this.parcelableMessageListener = listener
    }

    protected fun setSerializableMessageReceivedListener(listener: (key: String, message: Serializable) -> Unit) {
        this.serializableMessageListener = listener
    }

    protected fun setUnitMessageReceivedListener(listener: (key: String) -> Unit) {
        this.unitMessageListener = listener
    }

    private inline fun <P> doCoroutineWork(
        crossinline doOnAsyncBlock: suspend CoroutineScope.() -> P,
        coroutineScope: CoroutineScope,
        context: CoroutineContext
    ) {
        coroutineScope.launch {
            withContext(context) {
                doOnAsyncBlock.invoke(this)
            }
        }
    }
}
