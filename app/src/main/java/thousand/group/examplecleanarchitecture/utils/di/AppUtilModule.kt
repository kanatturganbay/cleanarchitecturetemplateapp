package thousand.group.examplecleanarchitecture.utils.di

import android.content.Context
import org.koin.dsl.module
import thousand.group.examplecleanarchitecture.utils.helpers.ExecHelper
import thousand.group.examplecleanarchitecture.utils.helpers.LocaleHelper
import thousand.group.examplecleanarchitecture.utils.helpers.MessageStatusHelper
import thousand.group.examplecleanarchitecture.utils.helpers.ProgressBarHelper
import thousand.group.examplecleanarchitecture.utils.system.ResourceManager

val appUtilModule = module {
    single { ExecHelper() }

    factory { (context: Context) -> ResourceManager(context) }
    factory { (context: Context) -> MessageStatusHelper(context, get()) }
    factory { (context: Context) -> ProgressBarHelper(context, get()) }
    factory { (context: Context) -> LocaleHelper(context, get()) }
}