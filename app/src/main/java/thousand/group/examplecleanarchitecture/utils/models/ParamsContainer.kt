package thousand.group.examplecleanarchitecture.utils.models

import android.os.Bundle
import android.os.Parcelable
import java.io.Serializable


class ParamsContainer(
) : Serializable {

    companion object {
        fun createParamsContainer(bundle: Bundle): ParamsContainer {
            val paramsContainer = ParamsContainer()

            if (!bundle.isEmpty) {
                bundle.keySet().forEachIndexed { index, key ->
                    val value = bundle.get(key)

                    when (value) {
                        is Int -> {
                            paramsContainer.putInt(key, value)
                        }
                        is String -> {
                            paramsContainer.putString(key, value)
                        }
                        is Boolean -> {
                            paramsContainer.putBoolean(key, value)
                        }
                        is Parcelable -> {
                            paramsContainer.putParcelable(key, value)
                        }
                        is Serializable -> {
                            paramsContainer.putSerializable(key, value)
                        }
                    }
                }
            }
            return paramsContainer
        }
    }

    private val paramsMap = mutableMapOf<String, Any>()

//    init {
//        bundle?.let {
//            if (!it.isEmpty) {
//                it.keySet().forEachIndexed { index, key ->
//                    val value = it.get(key)
//
//                    when (value) {
//                        is Int -> {
//                            putInt(key, value)
//                        }
//                        is String -> {
//                            putString(key, value)
//                        }
//                        is Boolean -> {
//                            putBoolean(key, value)
//                        }
//                        is Parcelable -> {
//                            putParcelable(key, value)
//                        }
//                        is Serializable -> {
//                            putSerializable(key, value)
//                        }
//                    }
//                }
//            }
//        }
//    }

    fun putInt(key: String, value: Int) = paramsMap.put(key, value)

    fun getInt(key: String) = paramsMap.get(key) as Int

    fun putString(key: String, value: String) = paramsMap.put(key, value)

    fun getString(key: String) = paramsMap.get(key) as String

    fun putBoolean(key: String, value: Boolean) = paramsMap.put(key, value)

    fun getBoolean(key: String) = paramsMap.get(key) as Boolean

    fun putParcelable(key: String, value: Parcelable) = paramsMap.put(key, value)

    fun getParcelable(key: String) = paramsMap.get(key) as Parcelable

    fun putSerializable(key: String, value: Serializable) = paramsMap.put(key, value)

    fun getSerializable(key: String) = paramsMap.get(key) as Serializable

    fun clear() {
        paramsMap.clear()
    }

    fun isExist(key: String) = paramsMap.get(key) != null

    fun isNotEmpty() = paramsMap.isNotEmpty()

}