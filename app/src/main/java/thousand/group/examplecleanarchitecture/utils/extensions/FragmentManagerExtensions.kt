package thousand.group.examplecleanarchitecture.utils.extensions

import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentManager
import androidx.fragment.app.FragmentTransaction

internal fun FragmentManager.removeFragment(tag: String) {
    this.findFragmentByTag(tag)?.let {
        this.beginTransaction()
            .remove(it)
            .commitNow()
        this.popBackStack()
    }
}

internal fun FragmentManager.addFragment(
    containerViewId: Int,
    fragment: Fragment,
    tag: String
) {
    this.beginTransaction()
        .disallowAddToBackStack()
        .add(containerViewId, fragment, tag)
        .commit()
}


internal fun FragmentManager.replaceFragment(
    containerViewId: Int,
    fragment: Fragment,
    tag: String
) {
    this.beginTransaction()
        .disallowAddToBackStack()
        .replace(containerViewId, fragment, tag)
        .commitAllowingStateLoss()
}

internal fun FragmentManager.replaceFragmentWithBackStack(
    containerViewId: Int,
    fragment: Fragment,
    tag: String
) {
    this.beginTransaction()
        .replace(containerViewId, fragment, tag)
        .addToBackStack(tag)
        .commitAllowingStateLoss()
}

internal fun FragmentManager.addFragmentWithBackStack(
    containerViewId: Int,
    fragment: Fragment,
    tag: String
) {
    this.beginTransaction()
        .add(containerViewId, fragment, tag)
        .addToBackStack(tag)
        .commitAllowingStateLoss()
}


inline fun FragmentManager.inTransaction(func: FragmentTransaction.() -> FragmentTransaction) =
    beginTransaction().func().commit()

internal fun FragmentManager.clearBackStack() {
    if (this.backStackEntryCount > 0) {
        this.popBackStack(null, FragmentManager.POP_BACK_STACK_INCLUSIVE)
    }
}

internal fun FragmentManager.clearAndReplaceFragment(
    containerViewId: Int,
    fragment: Fragment,
    tag: String
) {
    this.clearBackStack()
    this.beginTransaction()
        .disallowAddToBackStack()
        .replace(containerViewId, fragment, tag)
        .commitAllowingStateLoss()
}

internal fun FragmentManager.getCallerFragment(): String? = fragments.last().tag

internal fun FragmentManager.findFragment(tag: String): Fragment? = findFragmentByTag(tag)

internal fun Fragment.getSupportFragmentManager(): FragmentManager {
    return requireActivity().supportFragmentManager
}