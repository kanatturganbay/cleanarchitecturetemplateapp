package thousand.group.examplecleanarchitecture.views

import android.os.Bundle
import kotlinx.android.synthetic.main.fragment_user_info.*
import thousand.group.examplecleanarchitecture.R
import thousand.group.examplecleanarchitecture.utils.base.BaseBottomSheetFragment
import thousand.group.examplecleanarchitecture.utils.extensions.observeLiveData
import thousand.group.examplecleanarchitecture.utils.helpers.MainFragmentHelper
import thousand.group.examplecleanarchitecture.viewmodels.UserInfoViewModel

class UserInfoFragment : BaseBottomSheetFragment<UserInfoViewModel>(UserInfoViewModel::class) {
    override var layoutResId = R.layout.fragment_user_info

    override var fragmentMainTag = MainFragmentHelper.getJsonFragmentTag(
        MainFragmentHelper(
            title = fragmentTag,
            statusBarColorRes = R.color.colorAccent
        )
    )

    companion object {

        fun newInstance(): UserInfoFragment {
            val fragment = UserInfoFragment()
            val args = Bundle()

            //passing arguments
            fragment.arguments = args
            return fragment
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetText) {
            user_info_text.setText(it)
        }
    }

    override fun initController() {
        get_user_info_btn.setOnClickListener {
            viewModel.getUserInfo(
                login_edit.text.toString()
            )
        }
    }

    override fun internetSuccess() {

    }

    override fun internetError() {
    }

}