package thousand.group.examplecleanarchitecture.views

import android.os.Bundle
import kotlinx.android.synthetic.main.fragment_dialog_test_params.*
import thousand.group.examplecleanarchitecture.R
import thousand.group.examplecleanarchitecture.utils.base.BaseDialogFragment
import thousand.group.examplecleanarchitecture.utils.extensions.observeLiveData
import thousand.group.examplecleanarchitecture.utils.helpers.MainFragmentHelper
import thousand.group.examplecleanarchitecture.utils.models.LangMode
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer
import thousand.group.examplecleanarchitecture.viewmodels.TestParamsViewModel
import thousand.group.testknp.global.simple.AppConstants

class TestParamsDialogFragment :
    BaseDialogFragment<TestParamsViewModel>(TestParamsViewModel::class) {

    override var layoutResId = R.layout.fragment_dialog_test_params

    override var fragmentMainTag = MainFragmentHelper.getJsonFragmentTag(
        MainFragmentHelper(
            title = fragmentTag,
            statusBarColorRes = R.color.colorAccent
        )
    )

    companion object {

        fun newInstance(paramsContainer: ParamsContainer): TestParamsDialogFragment {
            val args = Bundle()

            //passing arguments
            args.putSerializable(AppConstants.BundleConstants.PARAMS_CONTAINER, paramsContainer)

            return createFragment(args)
        }

        fun newInstance(
            params1: Int,
            params2: String,
            params3: LangMode
        ): TestParamsDialogFragment {
            val args = Bundle()

            //passing arguments
            args.putInt(AppConstants.BundleConstants.params1, params1)
            args.putString(AppConstants.BundleConstants.params2, params2)
            args.putParcelable(AppConstants.BundleConstants.params3, params3)
            return createFragment(args)
        }

        private fun createFragment(args: Bundle): TestParamsDialogFragment {
            val fragment = TestParamsDialogFragment()
            fragment.arguments = args
            return fragment
        }
    }


    override fun initView(savedInstanceState: Bundle?) {
        viewModel.parseParams()
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetParams) {
            it?.apply {
                tv_params.setText(this)
            }
        }
    }

    override fun initController() {
    }

    override fun internetSuccess() {

    }

    override fun internetError() {
    }

}
