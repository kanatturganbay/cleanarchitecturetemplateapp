package thousand.group.examplecleanarchitecture.views

import android.os.Bundle
import kotlinx.android.synthetic.main.fragment_registration.*
import thousand.group.examplecleanarchitecture.R
import thousand.group.examplecleanarchitecture.utils.base.BaseBottomSheetFragment
import thousand.group.examplecleanarchitecture.utils.base.BaseFragment
import thousand.group.examplecleanarchitecture.utils.extensions.observeLiveData
import thousand.group.examplecleanarchitecture.utils.helpers.MainFragmentHelper
import thousand.group.examplecleanarchitecture.viewmodels.RegistrationViewModel

class RegistrationFragment : BaseBottomSheetFragment<RegistrationViewModel>(RegistrationViewModel::class) {
    override var layoutResId = R.layout.fragment_registration

    override var fragmentMainTag = MainFragmentHelper.getJsonFragmentTag(
        MainFragmentHelper(
            title = fragmentTag,
            statusBarColorRes = R.color.colorAccent
        )
    )

    companion object {

        fun newInstance(): RegistrationFragment {
            val fragment = RegistrationFragment()
            val args = Bundle()

            //passing arguments
            fragment.arguments = args
            return fragment
        }
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldSetData) {
            textView.setText("Good")
        }
    }

    override fun initController() {
        reg_up_btn.setOnClickListener {
            viewModel.regBtnClicked(
                login_edit.text.toString(),
                name_edit.text.toString(),
                surname_edit.text.toString(),
                email_edit.text.toString(),
                phone_edit.text.toString(),
                password_edit.text.toString(),
                status_edit.text.toString()
            )
        }
    }

    override fun internetSuccess() {

    }

    override fun internetError() {
    }

}