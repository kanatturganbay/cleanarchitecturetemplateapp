package thousand.group.examplecleanarchitecture.views

import android.os.Bundle
import kotlinx.android.synthetic.main.fragment_test_connectors.*
import thousand.group.examplecleanarchitecture.R
import thousand.group.examplecleanarchitecture.utils.base.BaseBottomSheetFragment
import thousand.group.examplecleanarchitecture.utils.helpers.MainFragmentHelper
import thousand.group.examplecleanarchitecture.viewmodels.TestConnectorsViewModel

class TestConnectorsFragment :
    BaseBottomSheetFragment<TestConnectorsViewModel>(TestConnectorsViewModel::class) {

    override var layoutResId = R.layout.fragment_test_connectors

    override var fragmentMainTag = MainFragmentHelper.getJsonFragmentTag(
        MainFragmentHelper(
            title = fragmentTag,
            statusBarColorRes = R.color.colorAccent
        )
    )

    companion object {

        fun newInstance(): TestConnectorsFragment {
            val fragment = TestConnectorsFragment()
            val args = Bundle()

            //passing arguments
            fragment.arguments = args
            return fragment
        }
    }

    override fun internetSuccess() {
    }

    override fun internetError() {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
    }

    override fun initController() {
        btn_send_parcelable.setOnClickListener {
            viewModel.sendParcelable()
        }

        btn_send_serializable.setOnClickListener {
            viewModel.sendSerializable()
        }

        btn_send_locale_callback.setOnClickListener {
            viewModel.sendCallback()
        }
        btn_send_string.setOnClickListener {
            viewModel.sendString()
        }
        btn_send_int.setOnClickListener {
            viewModel.sendInt()
        }
        btn_send_boolean.setOnClickListener {
            viewModel.sendBoolean()
        }
    }

}