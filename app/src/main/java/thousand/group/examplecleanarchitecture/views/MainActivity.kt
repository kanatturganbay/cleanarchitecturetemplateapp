package thousand.group.examplecleanarchitecture.views

import android.content.Intent
import android.os.Bundle
import kotlinx.android.synthetic.main.activity_main.*
import thousand.group.examplecleanarchitecture.R
import thousand.group.examplecleanarchitecture.utils.base.BaseActivity
import thousand.group.examplecleanarchitecture.utils.extensions.observeLiveData
import thousand.group.examplecleanarchitecture.utils.helpers.MainFragmentHelper
import thousand.group.examplecleanarchitecture.utils.models.LangMode
import thousand.group.examplecleanarchitecture.utils.models.ParamsContainer
import thousand.group.examplecleanarchitecture.viewmodels.MainViewModel

class MainActivity : BaseActivity<MainViewModel>(MainViewModel::class) {
    override var layoutResId = R.layout.activity_main

    override fun initIntent(intent: Intent?) {
    }

    override fun initView(savedInstanceState: Bundle?) {
    }

    override fun initLiveData() {
        observeLiveData(viewModel.ldRestartActivity) {
            finish()
            overridePendingTransition(0, 0)
            startActivity(Intent(this, MainActivity::class.java))
        }
        observeLiveData(viewModel.ldOpenTestParamsWithParams) {
            openTestParamsDialog(it)
        }
        observeLiveData(viewModel.ldOpenTestParamsWithParamsContainer) {
            openTestParamsDialog(it)
        }
        observeLiveData(viewModel.ldTestSimpleCallback){
            viewModel.showMessageToast("ldTestSimpleCallback")
        }
    }

    override fun initController() {
        reg_btn.setOnClickListener {
            openRegistrationFragment()
        }
        info_btn.setOnClickListener {
            openUserInfoFragment()
        }
        btn_change_lang.setOnClickListener {
            viewModel.changeLanguage()
        }
        btn_test_locale_storage.setOnClickListener {
            viewModel.testLocaleStorage()
        }
        btn_open_test_params.setOnClickListener {
            viewModel.openTestParamsWithParams()
        }
        btn_open_test_params_container.setOnClickListener {
            viewModel.openTestParamsWithParamsContainer()
        }
        btn_open_keyboard.setOnClickListener {
            viewModel.openKeyBoard()
        }
        btn_test_unit.setOnClickListener {
            viewModel.testSingleCallback()
        }
        btn_open_test_connector.setOnClickListener {
            openTestConnector()
        }

    }

    override fun fragmentLifeCycleController(tag: String) {
        val statusBarFlag = MainFragmentHelper.getStatusBarColorRes(tag)

        if (statusBarFlag != null) {
            changeStatusBarColor(statusBarFlag)
        } else {
            changeStatusBarColor(R.color.colorAccent)
        }
    }

    private fun openRegistrationFragment() {
        val fragment = RegistrationFragment.newInstance()

        fragment.show(supportFragmentManager, fragment.fragmentMainTag)
    }

    private fun openUserInfoFragment() {
        val fragment = UserInfoFragment.newInstance()

        fragment.show(supportFragmentManager, fragment.fragmentMainTag)
    }

    private fun openTestParamsDialog(paramsContainer: ParamsContainer?) {
        paramsContainer?.apply {
            val dialogFragment = TestParamsDialogFragment.newInstance(paramsContainer)
            dialogFragment.show(supportFragmentManager, dialogFragment.fragmentMainTag)
        }
    }

    private fun openTestParamsDialog(
        params: Triple<Int, String, LangMode>?
    ) {
        params?.apply {
            val dialogFragment =
                TestParamsDialogFragment.newInstance(params.first, params.second, params.third)

            dialogFragment.show(supportFragmentManager, dialogFragment.fragmentMainTag)
        }
    }

    private fun openTestConnector() {
            val dialogFragment = TestConnectorsFragment.newInstance()
            dialogFragment.show(supportFragmentManager, dialogFragment.fragmentMainTag)
    }

    override fun internetSuccess() {

    }

    override fun internetError() {
    }
}